@extends('layouts.app')
@section('content')
<div class="flex bg-gray-100 border-b border-gray-300 py-4">
    <div class="container mx-auto flex justify-between">
        <div class="flex">
            <router-link class="mr-4" to='/' exact>Home</router-link>
            <router-link to='/about'>About</router-link>
        </div>
        <div class="flex">

            @if(Auth::check())
                <router-link to='/dashboard'>Dashboard
                </router-link>
            @else
                <router-link class="mr-4" to='/login' exact>Login</router-link>
                <router-link to='/register'>Register</router-link>
            @endif
        </div>

    </div>
</div>
@if(Auth::check())
<div class="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
    <h1 class="text-white display-4">Vue Js Todo APP</h1>
    <br><br>
    <todo-component></todo-component>
</div>
@endif
        <div class="container mx-auto py-2">
            <router-view></router-view>
        </div>


        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
@endsection
